# Babashka Scripts

Miscellaneous [babashka](https://babashka.org "babashka website") scripts written for my own personal use.

## Requirements

babashka (>= 0.4.0)

## Usage

Clone this repository.

``` sh
git clone https://njoseph.me/gitweb/babashka-scripts.git
```

The list of available commands can be found by running `bb tasks` in the directory where the file `bb.edn` is present.

``` sh
The following tasks are available:

gup                Do a git pull and rebase branch with master in a given directory.
gpa                Runs `git pull` on all the git repositories in a given directory.
ebook-to-audiobook A utility to listen to your ebooks using TTS programs. Only tested on macOS with epub files.
install-deb        Install a Debian package given a direct URL to the .deb file.
install-deb-gh     Install a Debian package given a GitHub URL.
```

Some tasks can also take arguments.

``` sh
bb ebook-to-audiobook the-great-gatsby.epub
```

## Shell aliases

``` sh
# bash, zsh
source /path/to/babashka-scripts/bb-aliases.sh
alias task = bb --config ~/dev/babashka-scripts/bb.edn

# nu
source /path/to/babashka-scripts/bb-aliases.nu
alias task = bb --config ~/dev/babashka-scripts/bb.edn
```

*Example:* To pull all the repositories in a directory, run

``` sh
task gpa .
```

There is a utility `bbe` to run tasks in the current working directory in `bb-aliases.sh`.


*Example:* To pull all the repositories in a directory, run

``` sh
bbe gpa
```
