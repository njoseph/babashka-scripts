(ns debian
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [cheshire.core :as json]
            [org.httpkit.client :as http]
            [lib :refer [download-binary run-cmd]]))

(defn install-deb-from-url
  "Install a Debian package given a direct URL to the .deb file."
  [url]
  (println "Downloading deb package...")
  (download-binary url "package.deb")
  (println "Installing...")
  (println (run-cmd ["sudo" "apt" "install" "./package.deb"]))
  (println "Cleaning up..")
  (io/delete-file "package.deb")
  (println "Done."))

(defn install-deb-from-GitHub
  "Install a Debian package given a GitHub URL."
  [url]

  (assert (.contains url "github.com"))

  (letfn [(extract-GitHub-author-project
            ;; Extracts a string like author/project from URL.
            [url]
            (str/join "/" (subvec (str/split url #"/") 3)))
          (make-api-url ;; just some string interpolation
            [author-project]
            (str "https://api.github.com/repos/"
                 author-project
                 "/releases/latest"))
          (get-assets ;; Download list of assets from GitHub
            [api-url]
            (:assets (json/parse-string
                      (:body @(http/get api-url)) true)))
          (find-url-to-deb ;; Find Debian package URL from assets
            [assets]
            (some #(if (str/ends-with? % "deb") %)
                  (map #(:browser_download_url %) assets)))]

    (let [author-project (extract-GitHub-author-project url)
          api-url (make-api-url author-project)
          assets (get-assets api-url)
          deb-url (find-url-to-deb assets)]
      ;; Download and install Debian package
      (install-deb-from-url deb-url))))
