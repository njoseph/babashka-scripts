(ns ebooks
  (:require [clojure.java.io :as io]
            [clojure.string :refer [split]]
            [lib :refer [run-cmd extract-file-from-zip]]))

; Utilities for dealing with ebooks
;
; Dependencies:
; 1. calibre
; 2. ffmpeg
; 3. OS-dependent TTS software

;; TODO Check if all required utilities are installed

;; TODO Use tools.cli to provide more options
(defn convert-ebook-to-audiobook
  "A utility to listen to your ebooks using TTS programs. This utility only works on macOS for now."
  [book-file]
  ;; TODO Allow voice selection
  (let [default-voice (run-cmd ["defaults" "read" "com.apple.speech.voice.prefs" "SelectedVoiceName"])]
    (println (str "Will read using the default voice " default-voice)))
  (let [title (first (split book-file #"\."))
        txtz-file (str title ".txtz")
        txt-file (str title ".txt")
        aiff-file (str title ".aiff")
        mp3-file (str title ".mp3")]
    (println "Converting to text...")
    (run-cmd ["ebook-convert" book-file txtz-file])
    (extract-file-from-zip txtz-file "index.txt" txt-file)
    (println "Converting text to audio. Don't hold your breath!")
    ;; TODO Use festival-tts or say depending on OS
    (run-cmd ["say" "-f" txt-file "-o" aiff-file])
    (println "Creating mp3 file...")
    (run-cmd ["ffmpeg" "-i" aiff-file mp3-file])
    (println "Cleaning up...")
    (run! io/delete-file [txtz-file txt-file aiff-file])
    (println (str "Done! Check out " mp3-file))))
