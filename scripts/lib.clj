; Common utility functions used by scripts
; Unlike the functions in utils.clj, these are not directly useful

(ns lib
  (:require [babashka.process :as p]
            [org.httpkit.client :as http]
            [clojure.java.io :as io]
            [clojure.string :as str])
  (:import [java.nio.file Files FileSystems CopyOption StandardCopyOption]))

(defn run-cmd
  [command]
  (->> command
       p/process
       :out
       slurp))

(defn extract-file-from-zip
  [zip-file-name source destination]
  (let [zip-file (io/file zip-file-name)
        dest (io/file destination)
        fs (FileSystems/newFileSystem (.toPath zip-file) nil)
        file-in-zip (.getPath fs source (into-array String []))]
    (Files/copy file-in-zip
                (.toPath dest)
                (into-array CopyOption [StandardCopyOption/REPLACE_EXISTING]))))

(defn expand-home
  [s]
  (if (.startsWith s "~")
    (str/replace-first s "~" (System/getProperty "user.home"))
    s))

(defn download-binary
  [url destination]
  (io/copy (:body @(http/get url)) (io/file destination)))

(defn unixify
  "Make a function `f` behave like a UNIX shell command.

  Examples:

  # Multiple arguments
  $ install-deb url1 url2 url3

  # Read from file with one url on each line
  $ install-deb `cat urls.txt`

  # Piping from another process
  $ ls | install-deb  # Note: install-deb doesn't work with ls
  "
  [f]
  (when (= *file* (System/getProperty "babashka.file"))
    (if (> (.available System/in) 0)
      (->> (slurp *in*)
           (str/split-lines)
           (filter seq)
           (run! f))
      (->> (vec *command-line-args*)
           (map #(str/split % #" "))
           (flatten)
           (run! f)))))
