(ns utils
  (:require [lib :refer [expand-home run-cmd]]
            [babashka.process :refer [process]]))

(defn run-seq
  "Run a sequence of shell commands on a sequence of arguments.

  Examples:

  1. Play and delete each video from a folder
     ls | bb -i '(run-seq [\"mpv\" \"rm\"] *input*)'
  "
  [commands arguments]
  (doseq [argument arguments
          command commands]
    (println (run-cmd [command argument]))))

(defn run-task-in-cwd
  "Run a bb task from this repository in the current working directory."
  [task]
  (let [current-dir (System/getenv "PWD")
        babashka-scripts-dir (expand-home "~/dev/babashka-scripts/")]
    (print (-> (process ["bb" task current-dir] {:dir babashka-scripts-dir})
               :out
               slurp))))
